import java.util.*;
import java.io.IOException;
import java.nio.file.*;

public class CourseReader {
    public static void main(String[] args){
        try{
            //Polymorphism I changed List<String> for ArrayList<String>
            List<String> courses = (ArrayList<String>)getCourse("courses.csv");
            for (String string : courses) {
                System.out.println(string);
            }

            System.out.println("-----------------");
            //var automatically attribute List<String> type to coursesTaught
            var coursesTaught = getCoursesTaught("courses.csv", "Dr. Professorton");
            for (String string : coursesTaught) {
                System.out.println(string);
            }

            System.out.println("-----------------");
            String department = getDepartment("courses.csv", "Advanced Data Structures");
            System.out.println(department);

            System.out.println("-----------------");
            String room = getRoom("courses.csv", "Advanced Data Structures");
            System.out.println(room);
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static List<String> getCourse(String filename) throws IOException{
        Path path = Paths.get(filename);
        List<String> lines = Files.readAllLines(path);

        List<String> courses = new ArrayList<String>();
        for(int i=0; i<lines.size(); i++){
            String[] piece = lines.get(i).split(",");
            if(!piece[0].equals("group")){
                if(!courses.contains(piece[1])){
                    courses.add(piece[1]);
                }
            }
        }
        return courses;
    }

    public static List<String> getCoursesTaught(String filename, String teacherName) throws IOException{
        Path path = Paths.get(filename);
        List<String> lines = Files.readAllLines(path);

        List<String> coursesTaught = new ArrayList<String>();
        for(int i=0; i<lines.size(); i++){
            String[] piece = lines.get(i).split(",");
            if (piece[2].equals(teacherName)){
                coursesTaught.add(piece[1]);
            }
        }
        return coursesTaught;
    }

    public static String getDepartment(String filename, String course) throws IOException{
        Path path = Paths.get(filename);
        List<String> lines = Files.readAllLines(path);

        for(int i=0; i<lines.size(); i++){
            String[] piece = lines.get(i).split(",");
            if(piece[1].equals(course)){
               return piece[3];  
            }
        }
        return "Department not found for [" + course + "]";
    }

    public static String getRoom(String filename, String course) throws IOException{
        Path path = Paths.get(filename);
        List<String> lines = Files.readAllLines(path);

        for(int i=0; i<lines.size(); i++){
            String[] piece = lines.get(i).split(",");
            if(piece[1].equals(course)){
                if(piece[0].equals(piece[0])){
                    return piece[4];
                }
            }
        }
        return "Room not found for [" + course + "]";
    }
}
