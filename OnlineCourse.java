public class OnlineCourse implements Meeting{
    
    private String name;
    private String teacherName;
    private String department;
    private String room;

    public OnlineCourse(String name, String teacherName, String department, String room) {
        this.name = name;
        this.teacherName = teacherName;
        this.department = department;
        this.room = room;
    }

    public String getRoom() {
        return room;
    }

    public String getDepartment() {
        return department;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public String getName() {
        return name;
    }

    
}
