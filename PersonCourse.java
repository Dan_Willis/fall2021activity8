public class PersonCourse implements Meeting{

    private String name;
    private String teacherName;
    private String department;

    public PersonCourse(String name, String teacherName, String department) {
        this.name = name;
        this.teacherName = teacherName;
        this.department = department;
    }
    
    public String getDepartment() {
        return department;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public String getName() {
        return name;
    }

}
