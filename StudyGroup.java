public class StudyGroup implements Meeting{

    private String name;
    private String room;
    
    public StudyGroup(String name, String room) {
        this.name = name;
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public String getRoom() {
        return room;
    }
    
}
