import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;

public class TestMeeting {
    
    @Test
    public void testGetCourses(){
        try{
            var courses = CourseReader.getCourse("courses.csv");
            List<String> expectedCourses = new ArrayList<String>(){
                {
                    add("Anatomy 1");
                    add("Anatomy 2");
                    add("Cellular Biology");
                    add("Taxonomy");
                    add("Structural Biology");
                    add("Aeronautical engineering");
                    add("Digital Electronics");
                    add("Assembly Programming");
                    add("Advanced Data Structures");
                    add("Study Techniques");
                    add("Renaissance Literature");
                }
            };
            assertEquals(expectedCourses, courses);
        }catch(IOException e){
            System.out.println(e.getMessage());
        }          
    }
}
